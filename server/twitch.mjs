import dotenv from 'dotenv';
import axios from 'axios';

dotenv.config();

const api = axios.create({
  baseURL: 'https://api.twitch.tv/helix/',
  timeout: 20000,
  headers: {'Client-ID': process.env.TWITCH_CLIENT_ID}
});

export default api;