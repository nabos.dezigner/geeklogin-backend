import dotenv from 'dotenv';
import mongoose from 'mongoose';

dotenv.config();

export default () => mongoose.connect(process.env.MONGO_SERVER, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  dbName: 'SurvGame',
});

const dbConnection = mongoose.connection;
dbConnection.on('error', error => console.log(`Connection error:  ${error}`));
dbConnection.once('open', () => console.log(`Connected to DB!`));