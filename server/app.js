import express from 'express';
import cors from 'cors';
import graphqlHTTP from "express-graphql";
import schema from '../schema/index.mjs';
import ConnectToDataBase from './db.mjs';

const app = express();
const PORT = 9091;

ConnectToDataBase();

app.use(cors());
app.use('/graphql', graphqlHTTP({
  schema,
  graphiql: true
}));
app.options('*', cors());


app.listen(PORT, error => {
  error ? console.log(error) : console.log('Server started!');
});