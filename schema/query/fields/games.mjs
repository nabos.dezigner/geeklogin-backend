import graphql from 'graphql';
import {game} from '../../../catalog/game.mjs';
import {GamesType} from '../../types/index.mjs';

const {GraphQLList} = graphql;

export const games = {
  type: GraphQLList(GamesType),
  resolve(parent, args) {
    return game
  }
};

export default games;