import streams from './streams.mjs';
import news_categories from './news-categories.mjs';
import news from './news.mjs';
import news_item from './news-item.mjs';
import tag from './tag.mjs';
import tags from './tags.mjs';
import games from './games.mjs';
import games_categories from './games-categories.mjs';

export {
  streams, news_categories, news,
  news_item, tag, tags,
  games, games_categories
};

export default {
  streams, news_categories, news,
  news_item, tag, tags,
  games, games_categories
};