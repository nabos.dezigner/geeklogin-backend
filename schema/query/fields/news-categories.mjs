import graphql from 'graphql';
import {NewsCategoriesType} from '../../types/index.mjs';
import {NewsCategories} from '../../../models/index.mjs';

const {GraphQLList} = graphql;

export const news_categories = {
  type: GraphQLList(NewsCategoriesType),
  resolve(parent, args) {
    return NewsCategories.find();
  }
};

export default news_categories;