import graphql from 'graphql';
import twitch from '../../../server/twitch.mjs';
import {StreamsType} from '../../types/index.mjs';
import {game as games} from '../../../catalog/game.mjs'

const {GraphQLList, GraphQLString, GraphQLInt} = graphql;
const {get} = twitch;

export const streams = {
  type: GraphQLList(StreamsType),
  args: {
    categories: {type: GraphQLList(GraphQLString)},
    first: {type: GraphQLInt},
    lang: {type: GraphQLString}
  },
  async resolve(parent, {
    categories,
    first = 1,
    lang = 'ru'
  }) {
    const request = async (id) => await get(`/streams?game_id=${id}&language=${lang}&first=${first}`);
    const findGame = games.filter(game => {
      for (let category of game.categories)
        if (categories.includes(category)) return true;
    });
    const response = await Promise.all(findGame.map(({twitch_id}) => request(twitch_id)));
    return response.map(e => e.data.data).flat(Infinity);
  }
};

export default streams;