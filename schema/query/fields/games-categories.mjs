import graphql from 'graphql';
import {CATEGORIES} from '../../../catalog/game.mjs';
import {GamesCategoriesType} from '../../types/index.mjs';

const {GraphQLList} = graphql;

export const games_categories = {
  type: GraphQLList(GamesCategoriesType),
  resolve(parent, args) {
    return CATEGORIES
  }
};

export default games_categories;