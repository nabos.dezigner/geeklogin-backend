import graphql from 'graphql';
import {TagsType} from '../../types/index.mjs';
import {Tags} from '../../../models/index.mjs';

const {GraphQLList} = graphql;

export const tags = {
  type: GraphQLList(TagsType),
  resolve(parent, args) {
    return Tags.find();
  }
};

export default tags;