import graphql from 'graphql';
import {NewsType} from '../../types/index.mjs';
import {News} from '../../../models/index.mjs';

const {GraphQLList} = graphql;

export const news = {
  type: GraphQLList(NewsType),
  resolve(parent, args) {
    return News.find();
  }
};

export default news;