import graphql from 'graphql';
import {NewsType} from '../../types/index.mjs';
import {News} from '../../../models/index.mjs';

const {GraphQLID} = graphql;

export const news_item = {
  type: NewsType,
  args: {id: {type: GraphQLID}},
  resolve(parent, args) {
    return News.findById(args.id);
  }
};

export default news_item;