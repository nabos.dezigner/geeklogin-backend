import graphql from 'graphql';
import {TagsType} from '../../types/index.mjs';
import {Tags} from '../../../models/index.mjs';

const {GraphQLID} = graphql;

export const tag = {
  type: TagsType,
  args: {id: {type: GraphQLID}},
  resolve(parent, args) {
    return Tags.findById(args.id);
  }
};

export default tag;