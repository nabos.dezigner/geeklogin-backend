import graphql from 'graphql';
import {default as fields} from './fields/index.mjs';

const {GraphQLObjectType} = graphql;

export const Query = new GraphQLObjectType({name: 'Query', fields});

export default Query;