import graphql from 'graphql';
import query from './query/index.mjs';
import mutation from "./mutation/index.mjs";

const {GraphQLSchema} = graphql;

export default new GraphQLSchema({query, mutation});