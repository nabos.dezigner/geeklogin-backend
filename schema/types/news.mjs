import graphql from 'graphql';
import {NewsCategories, Tags} from '../../models/index.mjs'
import {NewsCategoriesType, TagsType} from './index.mjs'

const {
  GraphQLObjectType, GraphQLString, GraphQLID,
  GraphQLList
} = graphql;

const NewsType = new GraphQLObjectType({
  name: 'News',
  fields: () => ({
    id: {type: GraphQLID},
    headline: {type: GraphQLString},
    categories: {
      type: NewsCategoriesType,
      resolve(parent, args) {
        return NewsCategories.findById(parent.categories);
      }
    },
    description: {type: GraphQLString},
    body: {type: GraphQLString},
    tags: {
      type: GraphQLList(TagsType),
      resolve(parent, args) {
        return Tags.find().then(tags => tags.filter(tag => parent.tags.includes(tag._id)));
      }
    },
    creator: {type: GraphQLString},
    creation_date: {type: GraphQLString},
  })
});

export default NewsType;