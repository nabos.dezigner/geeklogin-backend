import graphql from 'graphql';

const {GraphQLObjectType, GraphQLString, GraphQLList} = graphql;

export const StreamsType = new GraphQLObjectType({
  name: 'Streams',
  fields: () => ({
    id: {type: GraphQLString},
    user_id: {type: GraphQLString},
    user_name: {type: GraphQLString},
    game_id: {type: GraphQLString},
    type: {type: GraphQLString},
    title: {type: GraphQLString},
    viewer_count: {type: GraphQLString},
    started_at: {type: GraphQLString},
    language: {type: GraphQLString},
    thumbnail_url: {type: GraphQLString},
    tag_ids: {type: GraphQLList(GraphQLString)}
  })
});

export default StreamsType;