import NewsType from './news.mjs';
import TagsType from './tags.mjs';
import NewsCategoriesType from './news-categories.mjs';
import StreamsType from './streams.mjs';
import GamesType from './games.mjs';
import GamesCategoriesType from './games-categories.mjs';

export {
  NewsType, TagsType, NewsCategoriesType,
  StreamsType, GamesType, GamesCategoriesType
};

export default {
  NewsType, TagsType, NewsCategoriesType,
  StreamsType, GamesType, GamesCategoriesType
};