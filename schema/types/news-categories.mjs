import graphql from 'graphql';

const {GraphQLObjectType, GraphQLString, GraphQLID} = graphql;

export const NewsCategoriesType = new GraphQLObjectType({
  name: 'NewsCategories',
  fields: () => ({
    id: {type: GraphQLID},
    name: {type: GraphQLString}
  })
});

export default NewsCategoriesType;