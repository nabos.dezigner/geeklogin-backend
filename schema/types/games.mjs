import graphql from 'graphql';
import {GamesCategoriesType} from "./index.mjs";
import {CATEGORIES} from '../../catalog/game.mjs';

const {GraphQLObjectType, GraphQLString, GraphQLList} = graphql;

export const GamesType = new GraphQLObjectType({
  name: 'Games',
  fields: () => ({
    twitch_id: {type: GraphQLString},
    name: {type: GraphQLString},
    categories: {
      type: GraphQLList(GamesCategoriesType),
      resolve(parent, args) {
        return CATEGORIES.filter(({name}) => parent.categories.includes(name));
      }
    },
  })
});

export default GamesType;