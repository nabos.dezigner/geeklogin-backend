import graphql from 'graphql';

const {GraphQLObjectType, GraphQLString} = graphql;

export const GamesCategoriesType = new GraphQLObjectType({
  name: 'GamesCategories',
  fields: () => ({
    name: {type: GraphQLString},
    ru: {type: GraphQLString},
    en: {type: GraphQLString},
  })
});

export default GamesCategoriesType;