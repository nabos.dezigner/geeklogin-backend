import graphql from 'graphql';
import {News} from '../../models/index.mjs';
import {NewsType} from './index.mjs';

const {
  GraphQLObjectType, GraphQLString, GraphQLID,
  GraphQLList
} = graphql;

export const TagsType = new GraphQLObjectType({
  name: 'Tags',
  fields: () => ({
    id: {type: GraphQLID},
    tag: {type: GraphQLString},
    news: {
      type: GraphQLList(NewsType),
      resolve(parent, args) {
        return News.find().then(items => items.filter(
          item => item.tags.includes(parent.id)
        ));
      }
    },
  })
});

export default TagsType;