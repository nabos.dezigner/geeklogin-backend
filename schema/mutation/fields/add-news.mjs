import graphql from 'graphql';
import {NewsType} from '../../types/index.mjs';
import {News} from '../../../models/index.mjs';

const {GraphQLID, GraphQLString, GraphQLList} = graphql;

export const add_news = {
  type: NewsType,
    args: {
    categories: {type: GraphQLID},
    headline: {type: GraphQLString},
    description: {type: GraphQLString},
    body: {type: GraphQLString},
    tags: {
      type: GraphQLList(GraphQLID)
    },
    creator: {type: GraphQLString},
    creation_date: {type: GraphQLString}
  },
  resolve(parent, args) {
    const addNews = new News({
      categories: args.categories,
      headline: args.headline,
      description: args.description,
      body: args.body,
      tags: args.tags,
      creator: args.creator,
      creation_date: args.creation_date
    });
    return addNews.save();
  }
};

export default add_news;