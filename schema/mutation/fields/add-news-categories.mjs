import graphql from 'graphql';
import {NewsCategoriesType} from '../../types/index.mjs';
import {NewsCategories} from '../../../models/index.mjs';

const {GraphQLString} = graphql;

export const add_news_categories = {
  type: NewsCategoriesType,
    args: {
    name: {type: GraphQLString}
  },
  resolve(parent, args) {
    const newsCategories = new NewsCategories({
      name: args.name
    });
    return newsCategories.save();
  }
};

export default add_news_categories;