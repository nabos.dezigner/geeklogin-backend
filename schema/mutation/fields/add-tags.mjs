import graphql from 'graphql';
import {TagsType} from '../../types/index.mjs';
import {Tags} from '../../../models/index.mjs';

const {GraphQLString} = graphql;

export const add_tags = {
  type: TagsType,
    args: {
    tag: {type: GraphQLString}
  },
  resolve(parent, args) {
    const tag = new Tags({
      tag: args.tag
    });
    return tag.save();
  }
};

export default add_tags;