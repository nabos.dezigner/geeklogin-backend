import graphql from 'graphql';
import {default as fields} from './fields/index.mjs';
const {GraphQLObjectType} = graphql;

export const Mutation = new GraphQLObjectType({name: 'Mutation', fields});

export default Mutation;