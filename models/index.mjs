import NewsCategories from './categories.mjs';
import News from './news.mjs';
import Tags from './tags.mjs';

export {
  NewsCategories, News, Tags
}

export default {
  NewsCategories, News, Tags
}