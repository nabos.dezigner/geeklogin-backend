import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const categoriesSchema = new Schema({
  name: String,
});

export default model('news_categories', categoriesSchema);