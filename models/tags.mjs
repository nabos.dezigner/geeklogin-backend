import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const tagsSchema = new Schema({
  tag: String,
});

export default model('tags', tagsSchema);