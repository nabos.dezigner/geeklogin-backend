import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const newsSchema = new Schema({
  categories: String,
  headline: String,
  description: String,
  body: String,
  tags: [String],
  creator: String,
  creation_date: String
});

export default model('news', newsSchema);