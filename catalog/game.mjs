const CATEGORIES = [
  {
    name: 'OTHER',
    ru: 'Другие',
    en: 'Other'
  },
  {
    name: 'ACTION',
    ru: 'Боевик',
    en: 'Action'
  },
  {
    name: 'PUZZLE',
    ru: 'Головоломка',
    en: 'Puzzle'
  },
  {
    name: 'SPORT',
    ru: 'Спорт',
    en: 'Sport'
  },
  {
    name: 'STRATEGY',
    ru: 'Стратегия',
    en: 'Strategy'
  },
  {
    name: 'CARD_AND_BOARD',
    ru: 'Карточная или настольная',
    en: 'Card and Board'
  },
  {
    name: 'SHMUP',
    ru: 'Шмап',
    en: 'Shoot’em up'
  },
  {
    name: 'ADVENTURE',
    ru: 'Приключенческая',
    en: 'Adventure'
  },
  {
    name: 'ROLE_PLAY',
    ru: 'Ролевая игра',
    en: 'Role play'
  },
  {
    name: 'MULTICART',
    ru: 'Сборник',
    en: 'Multicart'
  },
  {
    name: 'RACING_AND_DRIVING',
    ru: 'Гонки и вождение',
    en: 'Racing & driving'
  },
  {
    name: 'FIRST_PERSON_SHOOTER',
    ru: 'Шутер от первого лица',
    en: 'First-person shooter'
  },
  {
    name: 'SHOOTER',
    ru: 'Шутер',
    en: 'Shooter'
  },
  {
    name: 'SIMULATION',
    ru: 'Симулятор',
    en: 'Simulation'
  },
  {
    name: 'THRILLER',
    ru: 'Триллер',
    en: 'Thriller'
  },
  {
    name: 'OPEN_WORLD',
    ru: 'Открытый мир',
    en: 'Open world'
  },
  {
    name: 'SURVIVAL',
    ru: 'Выживание',
    en: 'Survival'
  },
  {
    name: 'STEALTH',
    ru: 'Стелс',
    en: 'Stealth'
  },
];

const game = [
  {
    twitch_id: 1,
    name: "Breakfree",
    categories: ['OTHER']
  },
  {
    twitch_id: 2,
    name: "Hyperballoid Deluxe: Survival Pack",
    categories: ['ACTION', 'PUZZLE']
  },
  {
    twitch_id: 3,
    name: "Bass Avenger",
    categories: ['SPORT']
  },
  {
    twitch_id: 4,
    name: "The Chessmaster 2000",
    categories: ['STRATEGY', 'CARD_AND_BOARD']
  },
  {
    twitch_id: 5,
    name: "Desert Strike: Return to the Gulf",
    categories: ['SHMUP', 'ACTION']
  },
  {
    twitch_id: 6,
    name: "Camelot Warriors",
    categories: ['ACTION']
  },
  {
    twitch_id: 7,
    name: "Super Spy Hunter",
    categories: ['ACTION']
  },
  {
    twitch_id: 8,
    name: "Fritz 9: Play Chess",
    categories: ['STRATEGY']
  },
  {
    twitch_id: 9,
    name: "The Real Deal 2",
    categories: ['STRATEGY']
  },
  {
    twitch_id: 10,
    name: "Gothmog's Lair",
    categories: ['ADVENTURE']
  },
  {
    twitch_id: 11,
    name: "WWE SmackDown! vs. RAW 2007",
    categories: ['SPORT', 'ACTION']
  },
  {
    twitch_id: 12,
    name: "Blade & Sword",
    categories: ['ROLE_PLAY']
  },
  {
    twitch_id: 13,
    name: "Mary-Kate and Ashley: Sweet 16: Licensed to Drive",
    categories: ['SPORT', 'RACING_AND_DRIVING']
  },
  {
    twitch_id: 14,
    name: "Premier Manager 98",
    categories: ['SPORT']
  },
  {
    twitch_id: 15,
    name: "EGA-Roids",
    categories: ['SPORT']
  },
  {
    twitch_id: 32982,
    name: "Grand Theft Auto V",
    categories: ['RACING_AND_DRIVING', 'ACTION', 'OPEN_WORLD', 'STEALTH']
  },
  {
    twitch_id: 65632,
    name: "Dayz",
    categories: ['FIRST_PERSON_SHOOTER', 'SHOOTER', 'ROLE_PLAY', 'THRILLER', 'OPEN_WORLD', 'SURVIVAL']
  },
  {
    twitch_id: 263490,
    name: "Rust",
    categories: ['SHOOTER', 'ADVENTURE', 'ACTION', 'SURVIVAL']
  },
];

export {game, CATEGORIES}

export default {game, CATEGORIES};